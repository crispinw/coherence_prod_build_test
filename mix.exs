defmodule CoherenceDemo.Mixfile do
  use Mix.Project

  def project do
    [app: :coherence_demo,
     version: "0.0.1",
     elixir: "~> 1.4.0",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix, :gettext] ++ Mix.compilers,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: aliases(),
     deps: deps()]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [mod: {CoherenceDemo, []},
     applications: [
        :lager, # put this first to avoid logger errors
        :coherence,
        :comeonin,
        :corman,
        :cowboy,
        :ex_aws,
        :facebook,
        :faker,
        :gettext,
        :hackney,
        :httpoison,
        :logger,
        :oauth2,
        :phoenix,
        :phoenix_ecto,
        :phoenix_html,
        :phoenix_pubsub,
        :postgrex,
        :swoosh,
        :timex,
        :timex_ecto,
        :twilex,
        :tzdata
      ]]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:bureaucrat, "~> 0.1.4", only: [:dev, :test]},
      {:coherence, "~> 0.3.1"},
      {:comeonin, "~> 2.6.0"},
      {:cowboy, "~> 1.0.0"},
      {:distillery, "~> 1.4.0"},
      {:earmark, "~> 1.2.1"},
      {:ex_aws, "~> 1.1.2"},
      {:ex_machina, "~> 2.0.0"},
      {:excoveralls, "~> 0.5", only: :test},
      {:facebook, "~> 0.13.2"},
      {:faker, "~> 0.8.0"},
      {:geo, "~> 1.4.1"},
      {:gettext, "~> 0.13.1"},
      {:hackney, "1.8.0", override: true}, # version 1.6.6 breaks ExAws functionality and 1.7 causes incompatibility issues
      {:httpoison, "~> 0.11.2", override: true},
      {:jsx, "~> 2.8.0", override: true},
      {:lager, github: "basho/lager", tag: "3.2.4", override: true},
      {:mcd, github: "EchoTeam/mcd"},
      {:oauth2, "~> 0.9"},
      {:phoenix, "~> 1.2.3"},
      {:phoenix_ecto, "~> 3.2.0"},
      {:phoenix_html, "~> 2.9.3", override: true},
      {:phoenix_live_reload, "~> 1.0.8", only: :dev},
      {:phoenix_pubsub, "~> 1.0.1"},
      {:plug_session_memcached, github: "IgnatiusT/plug-session-memcached"},
      {:poison, "~> 3.1.0", override: true},
      {:postgrex, "~> 0.13.2"},
      {:swoosh, "~> 0.7.0"},
      {:timex, "~> 3.1.13", override: true},
      {:timex_ecto, "~> 3.1.1", override: true},
      {:transit, github: "isaiah/transit-erlang"},
      {:twilex, "~> 0.0.2", override: true}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup"],
     "test": ["ecto.create --quiet", "ecto.migrate", "test"]]
  end
end
